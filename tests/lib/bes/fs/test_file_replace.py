#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

from bes.testing.unit_test import unit_test
from bes.fs.file_replace import file_replace
from bes.fs.file_util import file_util
from bes.fs.temp_file import temp_file
import os.path as path

class test_file_replace(unit_test):

  __unit_test_data_dir__ = '${BES_TEST_DATA_DIR}/lib/bes/fs/file_replace'

  def test_file_replace_ascii(self):
    tmp_file = self._make_temp_replace_file('ascii.txt')
    replacements = {
      'This': 'That',
      'foo': 'bar',
    }
    file_replace.replace(tmp_file, replacements, backup = False, word_boundary = True)
    self.assertEqualIgnoreWhiteSpace('That is bar.\n', file_util.read(tmp_file, codec = 'utf-8'))
    
  def test_file_replace_utf8(self):
    tmp_file = self._make_temp_replace_file('utf8.txt')
    replacements = {
      'This': 'That',
    }
    file_replace.replace(tmp_file, replacements, backup = False, word_boundary = True)
    self.assertEqualIgnoreWhiteSpace(u'That is bér.\n', file_util.read(tmp_file, codec = 'utf-8'))
    
  def _make_temp_replace_file(self, filename):
    src_file = self.data_path(filename)
    tmp_dir = temp_file.make_temp_dir()
    tmp_file = path.join(tmp_dir, path.basename(src_file))
    file_util.copy(src_file, tmp_file)
    return tmp_file
    
if __name__ == '__main__':
  unit_test.main()
